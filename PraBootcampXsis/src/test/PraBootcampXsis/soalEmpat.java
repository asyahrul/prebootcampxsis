package test.PraBootcampXsis;

import java.util.Scanner;

public class soalEmpat {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
        double sum = 0, sumPorsi = 0, porsiKecil = 0;

        System.out.print("Laki-laki dewasa : ");
        int lkDewasa = input.nextInt();
        sum += lkDewasa;
        sumPorsi = lkDewasa * 2;

        System.out.print("Perempuan dewasa : ");
        int pDewasa = input.nextInt();
        sum += pDewasa;
        sumPorsi += pDewasa;

        System.out.print("Remaja : ");
        int remaja = input.nextInt();
        sum += remaja;
        sumPorsi +=remaja;

        System.out.print("Anak-anak: ");
        int anakAnak = input.nextInt();
        sum += anakAnak;
        sumPorsi += anakAnak * 0.5;
        
        System.out.print("Balita : ");
        int balita = input.nextInt();
        sum += balita;
        porsiKecil += balita;
        sumPorsi +=balita;

        if (sum % 2 != 0 && sum > 5){
            sumPorsi += pDewasa;
        }

        System.out.println(sumPorsi + " Porsi");
	}

}
