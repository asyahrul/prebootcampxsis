package test.PraBootcampXsis;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class soalSatu {

	public static void main(String[] args) throws ParseException {

		Scanner input = new Scanner(System.in);
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        
        //Example input 29/07/2020 13:30:55
        System.out.print("Tanggal dan jam masuk : ");
        String masukTgl = input.nextLine();
        Date formatMasuk = format.parse(masukTgl);

        System.out.print("Tanggal dan jam keluar : ");
        String keluarTgl = input.nextLine();
        Date formatKeluar = format.parse(keluarTgl);

        long beda = formatKeluar.getTime() - formatMasuk.getTime();
        long bedaHari = TimeUnit.MILLISECONDS.toDays(beda);
        long bedaJam = TimeUnit.MILLISECONDS.toHours(beda);
        long bedaMenit = TimeUnit.MILLISECONDS.toMinutes(beda);

        int durasiJam=(int)bedaJam;
        int durasiMenit = (int)bedaMenit - ((int)bedaJam * 60);
        int tarifJam =0;
        int tarifHari=0;
        
        // Jika menit lebih dari satu maka durasiJam + 1
        if(durasiMenit > 0){
            durasiJam = durasiJam +1;
        }

        // Durasi Tarif Hari
        if(durasiJam >= 24){
            durasiJam = durasiJam - 24;
            tarifHari = (int)bedaHari * 15000;
        } else {
            tarifHari = (int)bedaHari * 15000;
        }

        // Durasi Tarif Jam
        if (durasiJam >=8 && durasiJam <24){
            tarifJam = 8000;
        } else {
            tarifJam = durasiJam * 1000;
        }

        int totalTarif = tarifHari + tarifJam;

        System.out.println("Rp. "+totalTarif);
	}

}
